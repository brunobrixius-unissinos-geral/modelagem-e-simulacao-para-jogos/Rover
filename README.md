# Rover_ModelagemSimulacaoParaJogos - Em Desenvolvimento -

Projeto desenvolvido na Unisinos para a cadeira de Modelagem e Simulação para Jogos

## Getting Started

O projeto ainda esta em desenvolvimento e tem muito ainda o que ser modificado tanto no codigo quanto a documentação deste repositorio.

### Prerequisites

O projeto esta sendo desenvolvido utilizando Unity

```
Unity 2019.3.0b3
```


## O Que Esta Pronto

Explain how to run the automated tests for this system

### PetriNet

Lê arquivos .pflow - (XML) e cria uma rede Petri internamente.

### PetriNetView

Cria a visualização 3D da PetriNet gerada. É possivel iteragir com a PetriNet gerada utilizando o mouse para ativar as transições e também para adicionar ou remover Tokens

### CellularAutomataMap

Gera utilizando Gizmos da Unity uma visualização dum mapa procedural
