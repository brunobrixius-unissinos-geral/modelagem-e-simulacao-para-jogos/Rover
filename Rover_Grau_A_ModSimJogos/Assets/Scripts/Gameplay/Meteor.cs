﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meteor : MonoBehaviour
{
    public float fallSpeed = 10f;
    public float fallDistance = 10f;
    public Action<Vector2Int> ExplodedAt;

    float scaleFactor = 1;

    float scaleSize = 10f;
    private Vector2Int position;
    public void SpawnAtPosition(Vector2Int startingPosition)
    {
        position = startingPosition;
        scaleSize = UnityEngine.Random.Range(1, 25f);
        scaleFactor = scaleSize / 10f;
        transform.position = new Vector3(startingPosition.x, startingPosition.y, 0);
    }
    void Update()
    {
        if(fallDistance < 0)
        {
            ExplodedAt?.Invoke(position);
            Destroy(gameObject);
        }

        fallDistance -= fallSpeed * Time.deltaTime;
        float newScale = scaleSize - fallDistance * scaleFactor;
        transform.localScale = new Vector3(newScale, newScale, 1);
    }
}
