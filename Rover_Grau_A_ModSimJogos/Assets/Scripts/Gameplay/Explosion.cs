﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{

    public float explosionDuration = 1.5f;

    void Update()
    {
        if(explosionDuration <0)
        {
            Destroy(gameObject);
        }
        explosionDuration -= Time.deltaTime;
    }
}
