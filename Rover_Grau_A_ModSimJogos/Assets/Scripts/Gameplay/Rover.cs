﻿using UnityEngine;

public class Rover : MonoBehaviour
{

    public int Life;
    public int Fuel;
    public int Ammo;

    public Vector2Int Position;

    public void Initialize(Vector2Int startPositionOnTile)
    {
        Position = startPositionOnTile;
    }

    public void SetNewPosition(Vector2Int newPosition)
    {
        Position = newPosition;
        transform.position = new Vector3(Position.x + 0.5f, Position.y + 0.5f, 0);

    }

    public void MoveRight()
    {
        Position += new Vector2Int(1, 0);
        transform.position += new Vector3(1f, 0, 0);
    }

    public void MoveDown()
    {
        Position += Vector2Int.down;

        transform.position -= new Vector3(0, 1f, 0);
    }

    public void MoveUp()
    {
        Position += new Vector2Int(0, 1);

        transform.position += new Vector3(0, 1f, 0);
    }

    public void MoveLeft()
    {
        Position += new Vector2Int(-1, 0);
        transform.position += new Vector3(-1f, 0, 0);
    }

}
