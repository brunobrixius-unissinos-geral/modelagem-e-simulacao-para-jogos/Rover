﻿using PetriNetLib.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Xml;

namespace PetriNetLib
{
    public class PetriNetFactory
    {
        private static Dictionary<int, PlaceData> _placeDictionary;
        private static Dictionary<int, TransitionData> _transitionDictionary;
        private static List<ArcData> _arcs;

        internal static PetriNetData CreatePetriNetData(string inputFile)
        {
            var settings = new XmlReaderSettings();
            settings.IgnoreWhitespace = true;

            using (XmlReader reader = XmlReader.Create(inputFile, settings))
            {
                int x;
                int y;

                _placeDictionary = new Dictionary<int, PlaceData>();
                _transitionDictionary = new Dictionary<int, TransitionData>();
                _arcs = new List<ArcData>();

                reader.ReadToDescendant("place");

                //TODO Check for input error

                while (reader.EOF == false)
                {
                    switch (reader.Name)
                    {
                        case "place":
                            {
                                using (XmlReader inner = reader.ReadSubtree())
                                {
                                    inner.ReadToFollowing("id");

                                    var newPlaceData = new PlaceData();

                                    int.TryParse(inner.ReadInnerXml(), out newPlaceData.Id);
                                    int.TryParse(inner.ReadInnerXml(), out x);
                                    int.TryParse(inner.ReadInnerXml(), out y);
                                    newPlaceData.Label = inner.ReadInnerXml();
                                    int.TryParse(inner.ReadInnerXml(), out newPlaceData.Token);
                                    bool.TryParse(inner.ReadInnerXml(), out newPlaceData.IsStatic);
                                    newPlaceData.Position = new Vector3(x, y, 0);

                                    _placeDictionary.Add(newPlaceData.Id, newPlaceData);
                                    inner.Close();
                                }
                                break;
                            }
                        case "transition":
                            {
                                using (XmlReader inner = reader.ReadSubtree())
                                {
                                    inner.ReadToFollowing("id");

                                    var newTransitionData = new TransitionData();

                                    int.TryParse(inner.ReadInnerXml(), out newTransitionData.Id);
                                    int.TryParse(inner.ReadInnerXml(), out x);
                                    int.TryParse(inner.ReadInnerXml(), out y);
                                    newTransitionData.Label = inner.ReadInnerXml();
                                    newTransitionData.Position = new Vector3(x, y, 0);

                                    _transitionDictionary.Add(newTransitionData.Id, newTransitionData);
                                    inner.Close();
                                }
                                break;
                            }
                        case "arc":
                            {
                                using (XmlReader inner = reader.ReadSubtree())
                                {
                                    inner.ReadToFollowing("type");

                                    var newArcData = new ArcData();

                                    Enum.TryParse(inner.ReadInnerXml(), true, out newArcData.Arctype);
                                    int.TryParse(inner.ReadInnerXml(), out newArcData.SourceId);
                                    int.TryParse(inner.ReadInnerXml(), out newArcData.DestinationId);
                                    int.TryParse(inner.ReadInnerXml(), out newArcData.Multiplicity);

                                    _arcs.Add(newArcData);
                                    inner.Close();
                                }
                                break;
                            }
                    }
                    reader.Skip();
                }
            }
            return new PetriNetData(_placeDictionary, _transitionDictionary, _arcs);
        }

        public static PetriNet Initialize(string inputFile)
        {
            PetriNetData petriNetData = CreatePetriNetData(inputFile);

            PetriNet petriNet = new PetriNet();

            petriNet.FillPlaces(petriNetData.PlaceDictionary);
            petriNet.FillTransitions(petriNetData);
            petriNet.CreateIO(petriNetData);

            petriNet.CreatePetriNetBottomUp(petriNetData);
            petriNet.ClearTemporaryData();

            return petriNet;
        }

    }
    
    public class PetriNet
    {
        protected internal class PetriObject
        {
            public int ID;
            public Vector3 Position;
            public string Label;
        }

        protected internal class Place : PetriObject
        {
            public List<Action> Commands = new List<Action>();
            protected internal int Token { get; set; }

            public void SetToken(int value)
            {
                Token = value;
            }

            protected internal void TryFireAction()
            {
                if (Token > 0)
                {
                    foreach (Action command in Commands)
                    {
                        command.Invoke();// .ExecuteAction(Label, Token);
                    }
                    Token = 0;
                }
            }
        }

        protected internal class Transition : PetriObject
        {
            public int Layer = -1;
            protected internal List<Arc> InputArcs;
            protected internal List<Arc> OutputArcs;

            public Transition()
            {
            }

            internal Transition(TransitionData transitionData)
            {
                ID = transitionData.Id;
                Position = transitionData.Position;
                Label = transitionData.Label;
            }

            private void SetDestinations()
            {
                for (int i = 0; i < OutputArcs.Count; i++)
                {
                    OutputArcs[i].SetToken();
                }
            }

            #region Transition Execution

            public void StartTransition()
            {
                if (CanTransit())
                {
                    RemoveInputTokens();
                    SetDestinations();
                }
            }

            private void RemoveInputTokens()
            {
                for (int i = 0; i < InputArcs.Count; i++)
                {
                    InputArcs[i].RemoveSourceToken();
                }
            }

            private bool CanTransit()
            {
                bool canTransit = true;

                for (int i = 0; i < InputArcs.Count; i++)
                {
                    canTransit = canTransit && InputArcs[i].Execute();
                }

                return canTransit;
            }

            #endregion Transition Execution
        }

        protected internal class Arc
        {
            public Arctype Arctype;
            public int Multiplicity;

            public PetriObject Source;
            public PetriObject Destination;

            public Arc(ArcData arcData, PetriObject source, PetriObject destination)
            {
                Arctype = arcData.Arctype;
                Multiplicity = arcData.Multiplicity;
                Source = source;
                Destination = destination;
            }

            public void RemoveSourceToken()
            {
                var place = (Source as Place);
                switch (Arctype)
                {
                    case Arctype.Regular:

                        if (place.Token >= Multiplicity)
                        {
                            place.Token -= Multiplicity;
                            return;
                        }
                        else
                        {
                            return;
                        }
                    case Arctype.Reset:
                        place.Token = 0;
                        return;

                    case Arctype.Inhibitor:
                        return;

                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            public bool Execute()
            {
                var place = (Source as Place);
                switch (Arctype)
                {
                    case Arctype.Regular:

                        return place.Token >= Multiplicity;

                    case Arctype.Reset:
                        return true;

                    case Arctype.Inhibitor:
                        return place.Token == 0;

                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            public void SetToken()
            {
                switch (Arctype)
                {
                    case Arctype.Regular:
                        var place = (Destination as Place);
                        place.Token += Multiplicity;
                        return;

                    case Arctype.Reset:
                    case Arctype.Inhibitor:
                        return;
                }
            }
        }

        private Dictionary<string, Place> inputCommands = new Dictionary<string, Place>();
        private Dictionary<string, Place> outputCommands = new Dictionary<string, Place>();
        private List<int> clearInputs = new List<int>();

        private Dictionary<int, Place> PlaceDictionary;
        private Dictionary<int, Transition> TransitionDictionary;

        private List<Arc> arcs = new List<Arc>();

        private List<List<Transition>> transitionLayers = new List<List<Transition>>();//Zero é a Layer mais baixa, proximo do fim
        private List<int> visitedPlaces = new List<int>();

        //private int LayerStep = -1;
        //private int TransitionStep = 0;

        public void SetInputPlaceToken(string commandName, int value, bool shouldClear = true)
        {
            SetInputPlaceToken(inputCommands[commandName].ID, value, shouldClear);
        }

        public void SetInputPlaceToken(int id, int value, bool shouldClear = true)
        {
            PlaceDictionary[id].SetToken(value);
            if (shouldClear)
            {
                clearInputs.Add(id);
            }
        }

        public void SetOutputPlaceAction(string commandName, IEnumerable<Action> commands)
        {
            SetOutputPlaceAction(outputCommands[commandName].ID, commands);
        }

        public void SetOutputPlaceAction(int id, IEnumerable<Action> commands)
        {
            PlaceDictionary[id].Commands.AddRange(commands);
        }

        public void SetOutputPlaceAction(string commandName, Action command)
        {
            SetOutputPlaceAction(outputCommands[commandName].ID, command);
        }

        public void SetOutputPlaceAction(int id, Action command)
        {
            PlaceDictionary[id].Commands.Add(command);
        }

        public void ExecuteAuto()
        {
            Execute();

            CheckOutputs();
            ClearInputs();
        }

        public void Execute()
        {
            for (int i = transitionLayers.Count - 1; i > -1; i--)
            {
                for (int j = 0; j < transitionLayers[i].Count; j++)
                {
                    transitionLayers[i][j].StartTransition();
                }
            }
        }

        public void CheckOutputs()
        {
            foreach (var place in outputCommands.Values)
            {
                place.TryFireAction();
            }
        }

        public void ClearInputs()
        {
            foreach (int clearId in clearInputs)
            {
                PlaceDictionary[clearId].SetToken(0);
            }
        }

        //public void ExecuteTransitionStep()
        //{
        //    if (LayerStep == -1)
        //    {
        //        LayerStep = transitionLayers.Count - 1;
        //        TransitionStep = 0;
        //    }

        //    transitionLayers[LayerStep][TransitionStep].StartTransition();
        //    TransitionStep++;

        //    if (TransitionStep == transitionLayers[LayerStep].Count)
        //    {
        //        TransitionStep = 0;
        //        LayerStep--;
        //    }

        //    if (LayerStep == -1)
        //    {
        //        CheckOutputs();
        //    }
        //}

        //public void ExecuteLayerStep()
        //{
        //    if (LayerStep == -1)
        //    {
        //        LayerStep = transitionLayers.Count - 1;
        //    }

        //    for (int j = 0; j < transitionLayers[LayerStep].Count; j++)
        //    {
        //        transitionLayers[LayerStep][j].StartTransition();
        //    }

        //    LayerStep--;

        //    if (LayerStep == -1)
        //    {
        //        CheckOutputs();
        //    }
        //}

        //public void ExecuteTransition(int id)
        //{
        //    TransitionDictionary[id].StartTransition();
        //}

        public void AddToken(int id)
        {
            PlaceDictionary[id].Token += 1;
        }

        public void RemoveToken(int id)
        {
            if (PlaceDictionary[id].Token > 0)
            {
                PlaceDictionary[id].Token -= 1;
            }
        }

        #region Creation

        internal void FillTransitions(PetriNetData petriNetData)
        {
            TransitionDictionary = new Dictionary<int, Transition>();

            for (var index = 0; index < petriNetData.Arcs.Count; index++)
            {
                ArcData arcData = petriNetData.Arcs[index];

                if (PlaceDictionary.ContainsKey(arcData.SourceId)) continue;

                if (TransitionDictionary.ContainsKey(arcData.SourceId) == false)
                {
                    TransitionData transitionData = petriNetData.TransitionDictionary[arcData.SourceId];

                    Transition transition = new Transition(transitionData);

                    Arc arc = new Arc(arcData, transition, PlaceDictionary[arcData.DestinationId]);

                    transition.OutputArcs = new List<Arc>();
                    transition.OutputArcs.Add(arc);

                    arcs.Add(arc);

                    TransitionDictionary.Add(transition.ID, transition);
                }
                else
                {
                    Transition transition = TransitionDictionary[arcData.SourceId];

                    Arc arc = new Arc(arcData, transition, PlaceDictionary[arcData.DestinationId]);

                    if (transition.OutputArcs == null)
                        transition.OutputArcs = new List<Arc>();

                    transition.OutputArcs.Add(arc);
                    arcs.Add(arc);
                }
            }

            for (var index = 0; index < petriNetData.Arcs.Count; index++)
            {
                ArcData arcData = petriNetData.Arcs[index];
                if (PlaceDictionary.ContainsKey(arcData.DestinationId)) continue;

                if (TransitionDictionary.ContainsKey(arcData.DestinationId) == false)
                {
                    TransitionData transitionData = petriNetData.TransitionDictionary[arcData.DestinationId];

                    Transition transition = new Transition(transitionData);

                    Arc arc = new Arc(arcData, PlaceDictionary[arcData.SourceId], transition);

                    transition.InputArcs = new List<Arc>();

                    transition.InputArcs.Add(arc);
                    arcs.Add(arc);

                    TransitionDictionary.Add(transition.ID, transition);
                }
                else
                {
                    Transition transition = TransitionDictionary[arcData.DestinationId];

                    Arc arc = new Arc(arcData, PlaceDictionary[arcData.SourceId], transition);

                    if (transition.InputArcs == null)
                        transition.InputArcs = new List<Arc>();

                    transition.InputArcs.Add(arc);
                    arcs.Add(arc);
                }
            }
        }

        internal void FillPlaces(Dictionary<int, PlaceData> placeDictionaryData)
        {
            PlaceDictionary = new Dictionary<int, Place>();
            foreach (var data in placeDictionaryData.Values)
            {
                Place newPlace = new Place();
                newPlace.ID = data.Id;
                newPlace.Position = data.Position;
                newPlace.Token = data.Token;
                newPlace.Label = data.Label;
                PlaceDictionary.Add(newPlace.ID, newPlace);
            }
        }

        internal void CreatePetriNetBottomUp(PetriNetData petriNetData)
        {
            int[] arcSourceIDs = petriNetData.Arcs.Select(x => x.SourceId).ToArray();
            List<int> outputPlaces = petriNetData.PlaceDictionary.Keys.Except(arcSourceIDs).ToList();

            foreach (int id in outputPlaces)
            {
                CreatePetriNetBottomUpRecursive(id, 0);
            }
        }

        internal void CreatePetriNetBottomUpRecursive(int id, int layerLevel)
        {
            List<Transition> listOfTransition = new List<Transition>();
            foreach (var transitionDictionaryValue in TransitionDictionary.Values)
            {
                for (int i = 0; i < transitionDictionaryValue.OutputArcs.Count; i++)
                {
                    if (transitionDictionaryValue.OutputArcs[i].Destination.ID == id)
                    {
                        listOfTransition.Add(transitionDictionaryValue);
                    }
                }
            }

            if (listOfTransition.Count > 0)
            {
                foreach (var transition in listOfTransition)
                {
                    if (transitionLayers.Count < layerLevel + 1)
                    {
                        transitionLayers.Add(new List<Transition>());
                    }

                    if (transition.Layer < layerLevel)
                    {
                        foreach (List<Transition> transitionLayer in transitionLayers)
                        {
                            if (transitionLayer.Remove(transitionLayer.Find(x => x.ID == transition.ID)))
                                break;
                        }
                        transitionLayers[layerLevel].Add(transition);
                        transition.Layer = layerLevel;
                    }
                    visitedPlaces.Add(id);

                    foreach (var innerTransition in listOfTransition)
                    {
                        foreach (var arc in innerTransition.InputArcs)
                        {
                            if (visitedPlaces.Contains(arc.Source.ID) == false)
                                CreatePetriNetBottomUpRecursive(arc.Source.ID, layerLevel + 1);
                        }
                    }
                }
            }
            else
            {
                visitedPlaces.Add(id);
            }
        }

        internal void CreateIO(PetriNetData petriNetData)
        {
            int[] arcSourceIDs = petriNetData.Arcs.Select(x => x.SourceId).ToArray();
            int[] arcDestiniIDs = petriNetData.Arcs.Select(x => x.DestinationId).ToArray();

            List<int> outputPlaces = petriNetData.PlaceDictionary.Keys.Except(arcSourceIDs).ToList();
            List<int> inputPlaces = petriNetData.PlaceDictionary.Keys.Except(arcDestiniIDs).ToList();

            for (int i = 0; i < outputPlaces.Count; i++)
            {
                outputCommands.Add(PlaceDictionary[outputPlaces[i]].Label, PlaceDictionary[outputPlaces[i]]);
            }

            for (int i = 0; i < inputPlaces.Count; i++)
            {
                inputCommands.Add(PlaceDictionary[inputPlaces[i]].Label, PlaceDictionary[inputPlaces[i]]);
            }
        }

        internal void ClearTemporaryData()
        {
            visitedPlaces.Clear();
        }

        #endregion Creation

        internal Dictionary<int, Place> GetPlaces()
        {
            return PlaceDictionary;
        }

        internal Dictionary<int, Transition> GetTransisitions()
        {
            return TransitionDictionary;
        }
    }

}
