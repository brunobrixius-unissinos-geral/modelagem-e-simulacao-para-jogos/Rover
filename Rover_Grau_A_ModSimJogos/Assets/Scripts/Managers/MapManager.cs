﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class MapManager : MonoBehaviour
{

    [SerializeField] private float perlinScaleMin = 2f;
    [SerializeField] private float perlinScaleMax = 5f;
    [SerializeField] private Tile[] tiles;
    [SerializeField] private float[] thresholds;
    [SerializeField] public Tilemap tilemap;


    [SerializeField] private Tile pathTile;
    [SerializeField] private Tile fireTile;
    [SerializeField] private Tile ashesTile;
    InfectionType[,] infectionMap;
    public Action<Vector2Int, int> OnInfectedTile;


    void Start()
    {
        tilemap = GetComponent<Tilemap>();
    }


    float resetTimer = 0;
    bool canReset = false;
    private void Update()
    {
        if (canReset && resetTimer > 1f)
        {
            ResetTiles();
        }
        resetTimer += Time.deltaTime;
    }

    private void ResetTiles()
    {
        for (int i = 0; i < pathToShow.Count; i++)
        {
            tilemap.SetTile(new Vector3Int(pathToShow[i].x, pathToShow[i].y, 0), oldTileBase[i]);
        }
        canReset = false;
    }

    TileBase[] oldTileBase;
    List<NesScripts.Point> pathToShow;
    public void SetDrawTile(List<NesScripts.Point> drawingTiles)
    {
        if(canReset) ResetTiles();
        oldTileBase = new TileBase[drawingTiles.Count];
        for (int i = 0; i < drawingTiles.Count; i++)
        {
            oldTileBase[i] = tilemap.GetTile(new Vector3Int(drawingTiles[i].x, drawingTiles[i].y, 0));
        }
        pathToShow = drawingTiles;
        ShowTiles();
        canReset = true;
        resetTimer = 0f;
    }
    private void ShowTiles()
    {
        foreach (var position in pathToShow)
        {
            tilemap.SetTile(new Vector3Int(position.x, position.y, 0), pathTile);
        }
    }
    private int width;
    private int height;
    public int[,] NewMap(int width, int height)
    {
        this.width = width;
        this.height = height;
        infectionMap = new InfectionType[width, height];

        int[,] weightMap = new int[width, height];

        float perlinScale = UnityEngine.Random.Range(perlinScaleMin, perlinScaleMax);

        tilemap.ClearAllTiles();

        PerlinNoiseGrid noise = new PerlinNoiseGrid(width, height, perlinScale);

        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                Vector3Int pos = new Vector3Int(i, j, 0);
                int tileWeight = GetIndex(noise[i, j]);
                weightMap[i, j] = tileWeight + 1;
                Tile t = tiles[Mathf.Clamp(tileWeight, 0, tiles.Length - 1)];
                tilemap.SetTile(pos, t);
            }
        }
        return weightMap;
    }
    public void SetRecoveredTile(Vector3Int position)
    {
        tilemap.SetTile(position, ashesTile);
        OnInfectedTile?.Invoke(new Vector2Int(position.x, position.y), 4);

    }

    public void SetInfectedTile(Vector3Int position)
    {
        tilemap.SetTile(position, fireTile);
        OnInfectedTile?.Invoke(new Vector2Int(position.x, position.y), int.MaxValue);
    }
    public void SetInfectionMap(InfectionType[,] infectionMap)
    {
        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                switch (infectionMap[x, y])
                {
                    case InfectionType.Infected:
                        tilemap.SetTile(new Vector3Int(x, y, 0), fireTile);
                        OnInfectedTile?.Invoke(new Vector2Int(x, y), int.MaxValue);
                        break;
                    case InfectionType.Recovered:
                        tilemap.SetTile(new Vector3Int(x, y, 0),ashesTile);
                        OnInfectedTile?.Invoke(new Vector2Int(x, y), 4);
                        break;
                    case InfectionType.NotSuceptible:
                    case InfectionType.Susceptible:
                    default:
                        break;  
                }
            }
        }
    }

    int GetIndex(float value)
    {
        for (int i = 0; i < thresholds.Length; i++)
        {
            if (value < thresholds[i])
            {
                return i;
            }
        }

        return int.MaxValue - 1;//tiles.Length - 1;
    }
}
