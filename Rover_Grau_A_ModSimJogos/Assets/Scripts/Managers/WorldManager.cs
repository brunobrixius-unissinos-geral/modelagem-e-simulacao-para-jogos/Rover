﻿using FloodSpill;
using FloodSpill.Utilities;
using System;
using System.Collections.Generic;
using UnityEngine;

public class WorldManager : MonoBehaviour
{
    public int MeteorMaxDistance = 50;
    public int SoldierCount = 10;
    public int SoldierMinDistance;

    int[,] worldWeights;
    int[,] floodFillMap;

    float[,] pathFindingMap;

    [SerializeField] GameObject ammoBoxGO;
    [SerializeField] GameObject fuelBoxGO;
    [SerializeField] GameObject portalGO;
    [SerializeField] GameObject robotGO;
    [SerializeField] GameObject soldierGO;

    [SerializeField] GameObject meteorGO;
    [SerializeField] GameObject redSignalGO;
    [SerializeField] GameObject explosionGO;

    private int worldMapWidth;
    private int worldMapHeight;
    private Vector2Int playerStartPosition;

    GameObject portal;
    Vector2Int portalPosition;

    public Action<Vector2Int> OnSpawnInfection;

    public Dictionary<Vector2Int, Collectable> Collectables = new Dictionary<Vector2Int, Collectable>();
    public Dictionary<Vector2Int, GameObject> MapAgents;

    public Dictionary<Vector2Int, GameObject> Soldiers = new Dictionary<Vector2Int, GameObject>();
    public Dictionary<Vector2Int, GameObject> Fuels = new Dictionary<Vector2Int, GameObject>();
    public Dictionary<Vector2Int, GameObject> AmmoBoxes = new Dictionary<Vector2Int, GameObject>();

    public void InitializeWorld(int[,] worldWeights, int width, int height)
    {
        ResetAll();
        this.worldWeights = worldWeights;
        worldMapWidth = width;
        worldMapHeight = height;
        SoldierMinDistance = (worldMapWidth + worldMapHeight) / 4;
    }

    public void ResetAll()
    {
        foreach (var soldier in Soldiers)
        {
            GameObject.Destroy(soldier.Value);
        }
        Soldiers?.Clear();

        foreach (var fuel in Fuels)
        {
            GameObject.Destroy(fuel.Value);
        }
        Fuels?.Clear();

        foreach (var ammo in AmmoBoxes)
        {
            GameObject.Destroy(ammo.Value);
        }
        AmmoBoxes?.Clear();

        Collectables?.Clear();
    }

    public void SpawnMeteor(Vector2Int playerPosition)
    {
        Debug.Log(playerPosition);
        //Seleciona uma posicao proxima do player para spawnar meteoro
        var nearPlayerPosition = GetPositionNearPoint(MeteorMaxDistance, playerPosition);

        //Spawna meteoro na posicao
        var newMeteor = Instantiate(meteorGO);
        newMeteor.GetComponent<Meteor>().SpawnAtPosition(nearPlayerPosition);
        newMeteor.GetComponent<Meteor>().ExplodedAt += HandleOnExplosion;
    }

    private void HandleOnExplosion(Vector2Int explosionPosition)
    {
        var newExplosion = Instantiate(explosionGO);
        newExplosion.transform.position = new Vector3(explosionPosition.x, explosionPosition.y);

        OnSpawnInfection?.Invoke(explosionPosition);
    }

    private void StartInfection(Vector2Int infectionStartPosition)
    {

    }

    private Vector2Int GetPositionNearPoint(int maxDistanceFromPoint, Vector2Int pointPosition)
    {
        Vector2Int position;

        while (true)
        {
            position = new Vector2Int(UnityEngine.Random.Range(-maxDistanceFromPoint, maxDistanceFromPoint), UnityEngine.Random.Range(-maxDistanceFromPoint, maxDistanceFromPoint)) + pointPosition;
            if (IsInsideBounds(position))
                return position;
        }
    }

    NesScripts.Grid PathGrid;

    public void PositionAgents()
    {
        Action<int, int, int> neighbourProcessor =
            (x, y, mark) => Debug.Log($"Processing {x}, {y} as a neighbour with {mark} mark.");

        Action<int, int> positionVisitor = (x, y) => Debug.Log($"Visiting {x}, {y}");

        Predicate<int, int> positionQualifier = (x, y) => GetTileWeight(x, y) < 5;


        var floodParameters = new FloodParameters(playerStartPosition.x, playerStartPosition.y)
        {
            Qualifier = positionQualifier,
            NeighbourProcessor = HandleProcessor,
            //SpreadingPositionVisitor = positionVisitor
        };
        floodFillMap = new int[worldMapWidth, worldMapHeight];
        pathFindingMap = new float[worldMapWidth, worldMapHeight];

        Array.Copy(worldWeights, floodFillMap, worldMapWidth * worldMapHeight);

        Array.Copy(worldWeights, pathFindingMap, worldMapWidth * worldMapHeight);
        for (int x = 0; x < worldMapWidth; x++)
        {
            for (int y = 0; y < worldMapHeight; y++)
            {
                if (pathFindingMap[x, y] > 5)
                    pathFindingMap[x, y] = 0;
            }
        }
        PathGrid = new NesScripts.Grid(pathFindingMap);

        new FloodSpiller().SpillFlood(floodParameters, floodFillMap);
        PositionPortal();
    }
    public List<NesScripts.Point> PathTo(Collectable collectable, Vector2Int playerPosition)
    {
        Vector2Int nearestPosition = new Vector2Int(int.MaxValue, int.MaxValue);

        switch (collectable)
        {
            case Collectable.AmmoBox:
                foreach (var ammo in AmmoBoxes)
                {
                    if (Vector2Int.Distance(ammo.Key, playerPosition) < Vector2Int.Distance(nearestPosition, playerPosition))
                    {
                        nearestPosition = ammo.Key;
                    }
                }
                break;
            case Collectable.FuelBox:
                foreach (var fuel in Fuels)
                {
                    if (Vector2Int.Distance(fuel.Key, playerPosition) < Vector2Int.Distance(nearestPosition, playerPosition))
                    {
                        nearestPosition = fuel.Key;
                    }
                }
                break;
            case Collectable.Soldier:
                foreach (var soldier in Soldiers)
                {
                    if (Vector2Int.Distance(soldier.Key, playerPosition) < Vector2Int.Distance(nearestPosition, playerPosition))
                    {
                        nearestPosition = soldier.Key;
                    }
                }
                break;
            case Collectable.Portal:
                nearestPosition = portalPosition;
                break;
            default:
                return null;
                break;
        }
        return PathTo(nearestPosition, playerPosition);
    }


    private List<NesScripts.Point> PathTo(Vector2Int position, Vector2Int playerPosition)
    {
        NesScripts.Point _from = new NesScripts.Point(playerPosition.x, playerPosition.y);

        NesScripts.Point _to = new NesScripts.Point(position.x, position.y);

        List<NesScripts.Point> path = NesScripts.Pathfinding.FindPath(PathGrid, _from, _to);

        return path;
    }

    private void PositionPortal()
    {
        var portalPosition = RandomValidPositionOnMap(playerStartPosition, 200);

        portal = Instantiate(portalGO);
        portal.transform.position = new Vector3(portalPosition.x + 0.5f, portalPosition.y + 0.5f);
        portal.transform.SetParent(transform);
        portalPosition = portalPosition;
        //Soldiers.Add(portalPosition, portal);
        Collectables.Add(portalPosition, Collectable.Portal);
    }

    //Posicionando diversos agentes ainda

    private void HandleProcessor(int x, int y, int mark)
    {
        PositionSoldiers(new Vector2Int(x, y));
        PositionFuel(new Vector2Int(x + (int)UnityEngine.Random.Range(-50, 50), y + (int)UnityEngine.Random.Range(-50, 50)));
        PositionAmmo(new Vector2Int(x + (int)UnityEngine.Random.Range(-20, 20), y + (int)UnityEngine.Random.Range(-20, 20)));
    }

    private void PositionAmmo(Vector2Int position)
    {
        foreach (var ammo in AmmoBoxes)
        {
            if (DistanceFromLastPosition(position, ammo.Key) < 100)
                return;
        }
        if (IsInsideBounds(position))
        {
            var ammo = Instantiate(ammoBoxGO);
            ammo.transform.position = new Vector3(position.x + 0.5f, position.y + 0.5f);
            ammo.transform.SetParent(transform);
            AmmoBoxes.Add(position, ammo);
            Collectables.Add(position, Collectable.AmmoBox);
        }
    }

    private void PositionSoldiers(Vector2Int position)
    {
        if (DistanceFromLastPosition(position, playerStartPosition) < 50)
            return;

        foreach (var soldier in Soldiers)
        {
            if (DistanceFromLastPosition(position, soldier.Key) < 150)
                return;
        }
        CreateNewSoldier(position);
    }

    private void CreateNewSoldier(Vector2Int position)
    {
        var soldier = Instantiate(soldierGO);
        soldier.transform.position = new Vector3(position.x + 0.5f, position.y + 0.5f);
        soldier.transform.SetParent(transform);
        Soldiers.Add(position, soldier);
        Collectables.Add(position, Collectable.Soldier);
    }

    private void PositionFuel(Vector2Int position)
    {
        foreach (var fuel in Fuels)
        {
            if (DistanceFromLastPosition(position, fuel.Key) < 200)
                return;
        }
        if (IsInsideBounds(position))
        {
            var fuel = Instantiate(fuelBoxGO);
            fuel.transform.position = new Vector3(position.x + 0.5f, position.y + 0.5f);
            fuel.transform.SetParent(transform);
            Fuels.Add(position, fuel);
            Collectables.Add(position, Collectable.FuelBox);
        }
    }

    public bool IsInsideBounds(Vector2Int position)
    {
        if (position.x > -1 && position.x < worldMapWidth && position.y < worldMapHeight && position.y > -1)
            return true;
        else
            return false;
    }
    public void SetWeightOnPosition(Vector2Int position,int newWeight)
    {
        worldWeights[position.x, position.y] = newWeight;
    }
    public int GetTileWeight(Vector2Int position)
    {
        var weight = worldWeights[position.x, position.y];
        return weight;
    }

    public int GetTileWeight(int x, int y)
    {
        return worldWeights[x, y];
    }

    private int DistanceFromPlayer(Vector2Int position)
    {
        return (int)Vector2Int.Distance(position, playerStartPosition);
    }

    private int DistanceFromLastPosition(Vector2Int position, Vector2Int lastPosition)
    {
        return (int)Vector2Int.Distance(lastPosition, position);
    }
    public Vector2Int NewPlayerPosition()
    {
        do
        {
            playerStartPosition = new Vector2Int(UnityEngine.Random.Range(0, worldMapWidth), UnityEngine.Random.Range(0, worldMapHeight));
        } while (GetTileWeight(playerStartPosition) > 100);
        return playerStartPosition;
    }

    public Collectable GetTileObject(Vector2Int position)
    {
        if (Collectables.ContainsKey(position))
        {
            var returnValue = Collectables[position];
            switch (returnValue)
            {
                case Collectable.AmmoBox:
                    GameObject.Destroy(AmmoBoxes[position].gameObject);
                    AmmoBoxes.Remove(position);
                    break;
                case Collectable.FuelBox:
                    GameObject.Destroy(Fuels[position].gameObject);
                    Fuels.Remove(position);
                    break;
                case Collectable.Soldier:
                    GameObject.Destroy(Soldiers[position].gameObject);
                    Soldiers.Remove(position);
                    break;
                case Collectable.Portal:
                    break;
                case Collectable.Empty:
                    break;
                default:
                    break;
            }
            Collectables.Remove(position);

            return returnValue;
        }
        return Collectable.Empty;
    }

    private Vector2Int RandomMapPosition()
    {
        return new Vector2Int(UnityEngine.Random.Range(0, worldMapWidth), UnityEngine.Random.Range(0, worldMapHeight));
    }

    private Vector2Int RandomValidPositionOnMap(Vector2Int awayFrom, int minDistance)
    {
        Vector2Int position;
        while (true)
        {
            position = new Vector2Int(UnityEngine.Random.Range(0, worldMapWidth), UnityEngine.Random.Range(0, worldMapHeight));
            if (position.x - awayFrom.x > minDistance && position.y - awayFrom.y > minDistance && GetTileWeight(position) < 5 && IsInsideBounds(position))
            {
                return position;
            }
            minDistance--;
        }
    }
}
