﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum InfectionType
{
    Susceptible,
    Infected,
    Recovered,
    NotSuceptible
}

public class InfectionManager : MonoBehaviour
{

    public InfectionType[,] infectionMap;

    private int width;
    private int height;

    public Action<InfectionType[,]> OnFinishExecution;
    public Action<Vector3Int> OnNewInfectedTile;
    public Action<Vector3Int> OnNewRecoveredTile;

    [SerializeField] private float beta = 0.05f;
    [SerializeField] private float gamma = 0.1f;

    public void Initialize(int[,] map, int width, int height)
    {
        this.width = width;
        this.height = height;
        infectionMap = new InfectionType[width, height];
        CreateInfectionMap(map);
    }

    public void SetInfection(Vector2Int position)
    {
        infectionMap[position.x, position.y] = InfectionType.Infected;
    }

    public void SetTile(InfectionType infectionType, Vector2Int position)
    {
        infectionMap[position.x, position.y] = infectionType;
    }

    public void InfectionExecution()
    {
        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                if(infectionMap[x, y] == InfectionType.Infected)
                {
                    ExposeNeighbors(x, y);
                    TryRecover(x, y);
                }
            }
        }
        //OnFinishExecution?.Invoke(infectionMap);
    }

    private void CreateInfectionMap(int[,] inputMap)
    {
        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                if (inputMap[x, y] > 5)
                    infectionMap[x, y] = InfectionType.NotSuceptible;
            }
        }
    }

    private void ExposeNeighbors(int x, int y)
    {
        TryInfection(x+1, y);
        TryInfection(x+1, y+1);
        TryInfection(x+1, y-1);

        TryInfection(x-1, y);
        TryInfection(x-1, y+1);
        TryInfection(x-1, y-1);
    }
    private bool IsOutOfBounds(int x, int y)
    {
        if (x < 0 || x > width)
            return true;
        if (y < 0 || y > height)
            return true;
        else
            return false;
    }
    private void TryInfection(int x, int y)
    {
        if(infectionMap[x, y] == InfectionType.Infected || IsOutOfBounds(x,y))
        {
            return;
        }
        var isInfectabl = infectionMap[x, y];
        if (UnityEngine.Random.Range(0f,1f) < beta && infectionMap[x, y] == InfectionType.Susceptible)
        {
            infectionMap[x, y] = InfectionType.Infected;
            OnNewInfectedTile?.Invoke(new Vector3Int(x, y,0));
        }
    }

    private void TryRecover(int x, int y)
    {
        if (UnityEngine.Random.Range(0f, 1f) < gamma)
        {
            infectionMap[x, y] = InfectionType.Recovered;
            OnNewRecoveredTile?.Invoke(new Vector3Int(x, y,0));
        }
    }
}
