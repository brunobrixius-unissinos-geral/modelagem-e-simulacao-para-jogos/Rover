﻿using PetriNetLib;
using System.Collections.Generic;
using UnityEngine;

public enum Direction
{
    Up,
    Down,
    Left,
    Right
}


public enum Collectable
{
    AmmoBox,
    FuelBox,
    Soldier,
    Portal,
    Empty
}
public class GameManager : MonoBehaviour
{

    [SerializeField] private MapManager mapManager;
    [SerializeField] private Rover rover;
    [SerializeField] private WorldManager worldManager;
    [SerializeField] private InfectionManager infectionManager;

    [SerializeField] private int width = 512;
    [SerializeField] private int height = 512;

    PetriNet roverPetriNet;

    // Start is called before the first frame update
    void Start()
    {
        var weightMap = mapManager.NewMap(width, height);

        worldManager.InitializeWorld(weightMap, width, height);
        infectionManager.Initialize(weightMap, width, height);

        rover.SetNewPosition(worldManager.NewPlayerPosition());
        worldManager.PositionAgents();

        var path = "/Rover.pflow";
        roverPetriNet = PetriNetFactory.Initialize(Application.streamingAssetsPath + path);

        InitializeCommands();
        worldManager.OnSpawnInfection += infectionManager.SetInfection;
        mapManager.OnInfectedTile += worldManager.SetWeightOnPosition;
        //infectionManager.OnFinishExecution += mapManager.SetInfectionMap;

        infectionManager.OnNewInfectedTile += mapManager.SetInfectedTile;
        infectionManager.OnNewRecoveredTile += mapManager.SetRecoveredTile;

        rover.Fuel = 50000;
        rover.Life = 500;

    }

    private void InitializeCommands()
    {
        //Player Commands
        roverPetriNet.SetOutputPlaceAction("GoRight", rover.MoveRight);
        roverPetriNet.SetOutputPlaceAction("GoLeft", rover.MoveLeft);
        roverPetriNet.SetOutputPlaceAction("GoUp", rover.MoveUp);
        roverPetriNet.SetOutputPlaceAction("GoDown", rover.MoveDown);



        //WorldMap Commands
        roverPetriNet.SetOutputPlaceAction("SoldierSaved", OnSoldierSaved);
        roverPetriNet.SetOutputPlaceAction("NewAmmo", OnNewAmmo);


    }

    private void OnNewAmmo()
    {
        Debug.Log($"OnNewAmmo");
    }

    private void OnSoldierSaved()
    {
        Debug.Log($"OnSoldierSaved");
    }

    private bool IsValidMove(Direction direction)
    {
        Vector2Int directionValue;
        switch (direction)
        {
            case Direction.Up:
                directionValue = Vector2Int.up;
                break;
            case Direction.Down:
                directionValue = Vector2Int.down;
                break;
            case Direction.Left:
                directionValue = Vector2Int.left;
                break;
            case Direction.Right:
                directionValue = Vector2Int.right;
                break;
            default:
                directionValue = Vector2Int.zero;
                break;
        }
        if (worldManager.IsInsideBounds(rover.Position + directionValue) == false)
            return false;

        var weight = worldManager.GetTileWeight(rover.Position + directionValue);

        if (rover.Fuel > weight)
        {
            rover.Fuel -= weight;
            return true;
        }

        return false;
    }

    List<NesScripts.Point> drawingTiles = new List<NesScripts.Point>();

    float infectionSpread = 0f;
    // Update is called once per frame
    float timer = 0f;
    void Update()
    {
        if(infectionSpread > 0.4f)
        {
            infectionManager.InfectionExecution();
            infectionSpread = 0f;
        }
        infectionSpread += Time.deltaTime;

        if (Input.GetKeyDown(KeyCode.T))
        {
            var path = worldManager.PathTo(Collectable.Soldier, rover.Position);

            mapManager.SetDrawTile(path);
        }
        else if (Input.GetKeyDown(KeyCode.Y))
        {
            var path = worldManager.PathTo(Collectable.AmmoBox, rover.Position);

            mapManager.SetDrawTile(path);
        }
        else if (Input.GetKeyDown(KeyCode.U))
        {
            var path = worldManager.PathTo(Collectable.FuelBox, rover.Position);

            mapManager.SetDrawTile(path);
        }
        else if (Input.GetKeyDown(KeyCode.I))
        {
        }
        else if (Input.GetKeyDown(KeyCode.O))
        {
            var path = worldManager.PathTo(Collectable.Portal, rover.Position);

            mapManager.SetDrawTile(path);
        }
        else if (Input.GetKeyDown(KeyCode.M))
        {
            worldManager.SpawnMeteor(rover.Position);
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            roverPetriNet.SetInputPlaceToken("TryCollect", 1);
            switch (worldManager.GetTileObject(rover.Position))
            {
                case Collectable.AmmoBox:
                    roverPetriNet.SetInputPlaceToken("OnAmmoBox", 1);
                    break;
                case Collectable.FuelBox:
                    roverPetriNet.SetInputPlaceToken("OnFuelBox", 1);
                    break;
                case Collectable.Soldier:
                    roverPetriNet.SetInputPlaceToken("OnSoldier", 1);
                    break;
                case Collectable.Portal:
                    roverPetriNet.SetInputPlaceToken("OnPortal", 1);
                    break;
                case Collectable.Empty:
                    break;
            }

        }
        if (timer > 0.1f)
        {
            if (Input.GetKey(KeyCode.W))
            {
                roverPetriNet.SetInputPlaceToken("MoveUp", 1);

                roverPetriNet.SetInputPlaceToken("MoveCommand", 1);
                if (IsValidMove(Direction.Up))
                {
                    roverPetriNet.SetInputPlaceToken("IsValidMove", 1);
                }
            }
            if (Input.GetKey(KeyCode.A))
            {
                roverPetriNet.SetInputPlaceToken("MoveLeft", 1);
                roverPetriNet.SetInputPlaceToken("MoveCommand", 1);
                if (IsValidMove(Direction.Left))
                {
                    roverPetriNet.SetInputPlaceToken("IsValidMove", 1);
                }
            }
            if (Input.GetKey(KeyCode.S))
            {
                roverPetriNet.SetInputPlaceToken("MoveDown", 1);
                roverPetriNet.SetInputPlaceToken("MoveCommand", 1);
                if (IsValidMove(Direction.Down))
                {
                    roverPetriNet.SetInputPlaceToken("IsValidMove", 1);
                }
            }
            if (Input.GetKey(KeyCode.D))
            {
                roverPetriNet.SetInputPlaceToken("MoveRight", 1);
                roverPetriNet.SetInputPlaceToken("MoveCommand", 1);
                if (IsValidMove(Direction.Right))
                {
                    roverPetriNet.SetInputPlaceToken("IsValidMove", 1);
                }
            }
            timer = 0;
        }
        timer += Time.deltaTime;

        roverPetriNet.ExecuteAuto();

    }
}
