﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Consumable : MonoBehaviour
{
    public void SetPosition(Vector2Int newPosition)
    {
        transform.position = new Vector3(newPosition.x+0.5f, newPosition.y+0.5f);
    }
}
