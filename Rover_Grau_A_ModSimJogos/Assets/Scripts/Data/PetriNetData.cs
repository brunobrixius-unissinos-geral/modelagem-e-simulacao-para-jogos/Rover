﻿using System.Collections.Generic;
using System.Numerics;

    namespace PetriNetLib.Data
{
    internal struct PetriNetData
    {
        public PetriNetData(Dictionary<int, PlaceData> placeDictionary, Dictionary<int, TransitionData> transitionDictionary, List<ArcData> arcs)
        {
            this.PlaceDictionary = placeDictionary;
            this.TransitionDictionary = transitionDictionary;
            this.Arcs = arcs;
        }

        public Dictionary<int, PlaceData> PlaceDictionary;
        public Dictionary<int, TransitionData> TransitionDictionary;
        public List<ArcData> Arcs;
    }

    internal class PetriObjectData
    {
        public int Id;
        public Vector3 Position;
        public string Label;
    }

    internal class TransitionData : PetriObjectData
    {
    }

    internal class PlaceData : PetriObjectData
    {
        public int Token;
        public bool IsStatic;
    }

    public class ArcData
    {
        public Arctype Arctype;
        public int SourceId;
        public int DestinationId;
        public int Multiplicity;
    }

    public enum Arctype
    {
        Regular,
        Reset,
        Inhibitor
    }

}